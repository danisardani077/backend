const router = require('express').Router();
const { program_studi } = require('../controllers');

// Ambil data program_studi
router.get('/program_studi', program_studi.getDataprogram_studi);

// Tambah data program_studi ke database
router.post('/program_studi/add', program_studi.addDataprogram_studi);

// edit data program_studi
router.post('/program_studi/edit', program_studi.editDataprogram_studi);

// Delete data program_studi
router.post('/program_studi/delete', program_studi.deleteDataprogram_studi);

module.exports = router;