const router = require('express').Router();
const { krs } = require('../controllers');

// ambil data mata kuliah per program studi
router.get('/view/mk', krs.getDatamk);

// ambil data sks mahasiswa
router.post('/view/sks', krs.addDatasks);


module.exports = router;