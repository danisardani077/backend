const router = require('express').Router();
const { mata_kuliah } = require('../controllers');

// Ambil data mata_kuliah
router.get('/mata_kuliah', mata_kuliah.getDatamata_kuliah);

// Tambah data mata_kuliah ke database
router.post('/mata_kuliah/add', mata_kuliah.addDatamata_kuliah);

// edit data mata_kuliah
router.post('/mata_kuliah/edit', mata_kuliah.editDatamata_kuliah);

// Delete data mata_kuliah
router.post('/mata_kuliah/delete', mata_kuliah.deleteDatamata_kuliah);

module.exports = router;