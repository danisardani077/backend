const router = require('express').Router();
const { krs } = require('../controllers');

// Ambil data krs
router.get('/krs', krs.getDatakrs);

// Tambah data krs ke database
router.post('/krs/add', krs.addDatakrs);

// edit data krs
router.post('/krs/edit', krs.editDatakrs);

// Delete data krs
router.post('/krs/delete', krs.deleteDatakrs);

module.exports = router;