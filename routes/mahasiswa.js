const router = require('express').Router();
const { mahasiswa } = require('../controllers');

// Ambil data mahasiswa
router.get('/mahasiswa', mahasiswa.getDatamahasiswa);

// Tambah data mahasiswa ke database
router.post('/mahasiswa/add', mahasiswa.addDatamahasiswa);

// edit data mahasiswa
router.post('/mahasiswa/edit', mahasiswa.editDatamahasiswa);

// Delete data mahasiswa
router.post('/mahasiswa/delete', mahasiswa.deleteDatamahasiswa);

module.exports = router;