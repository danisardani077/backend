const config = require('../database');
const mysql = require('mysql');
const pool = mysql.createPool(config);

pool.on('error',(err)=> {
    console.error(err);
});

module.exports ={
    // Ambil data semua mata_kuliah
    getDatamata_kuliah(req,res){
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                SELECT * FROM mata_kuliah;
                `
            , function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'data berhasil diambil',
                    data: results 
                });
            });
            connection.release();
        })
    },
    // Menambah data mata_kuliah
    addDatamata_kuliah(req,res){
        let data = {
            kode_mk : req.body.kode_mk,
            nama : req.body.nama,
            sks : req.body.sks,
            kode_ps : req.body.kode_ps
        }
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                INSERT INTO mata_kuliah SET ?;
                `
            , [data],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'Data berhasil ditambahkan',
                });
            });
            connection.release();
        })
    },
    // edit data mata_kuliah
    editDatamata_kuliah(req,res){
        let dataEdit = {
            kode_mk : req.body.kode_mk,
            nama : req.body.nama,
            sks : req.body.sks,
            kode_ps : req.body.kode_ps
        }
        let kode_mk = req.body.kode_mk
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                UPDATE mata_kuliah SET ? WHERE kode_mk = ?;
                `
            , [dataEdit, kode_mk],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'data berhasil diedit',
                });
            });
            connection.release();
        })
    },
    // Delete data mata_kuliah
    deleteDatamata_kuliah(req,res){
        let kode_mk = req.body.kode_mk
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                DELETE FROM mata_kuliah WHERE kode_mk = ?;
                `
            , [kode_mk],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'Data berhasil dihapus'
                });
            });
            connection.release();
        })
    }
}