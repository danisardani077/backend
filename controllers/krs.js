const config = require('../database');
const mysql = require('mysql');
const pool = mysql.createPool(config);

pool.on('error',(err)=> {
    console.error(err);
});

module.exports ={
    // Ambil data semua krs
    getDatakrs(req,res){
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                SELECT * FROM krs;
                `
            , function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'data berhasil diambil',
                    data: results 
                });
            });
            connection.release();
        })
    },
    // Menambah data krs
    addDatakrs(req,res){
        let data = {
            nim : req.body.nim,
            kode_mk : req.body.kode_mk
        }
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                INSERT INTO krs SET ?;
                `
            , [data],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'Data berhasil ditambahkan',
                });
            });
            connection.release();
        })
    },
    // edit data krs
    editDatakrs(req,res){
        let dataEdit = {
            nim : req.body.nim,
            kode_mk : req.body.kode_mk
        }
        let nim = req.body.nim
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                UPDATE krs SET ? WHERE nim = ?;
                `
            , [dataEdit, nim],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'data berhasil diedit',
                });
            });
            connection.release();
        })
    },
    // Delete data krs
    deleteDatakrs(req,res){
        let nim = req.body.nim
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                DELETE FROM krs WHERE nim = ?;
                `
            , [nim],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'Data berhasil dihapus'
                });
            });
            connection.release();
        })
    }
}