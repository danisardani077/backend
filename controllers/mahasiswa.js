const config = require('../database');
const mysql = require('mysql');
const pool = mysql.createPool(config);

pool.on('error',(err)=> {
    console.error(err);
});

module.exports ={
    // Ambil data semua mahasiswa
    getDatamahasiswa(req,res){
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                SELECT * FROM mahasiswa;
                `
            , function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'data berhasil diambil',
                    data: results 
                });
            });
            connection.release();
        })
    },
    // Menambah data mahasiswa
    addDatamahasiswa(req,res){
        let data = {
            nama : req.body.nama,
            nim : req.body.nim,
            tanggal_lahir : req.body.tanggal_lahir,
            tempat_lahir : req.body.tempat_lahir,
            tahun_masuk : req.body.tahun_masuk,
            kode_ps : req.body.kode_ps
        }
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                INSERT INTO mahasiswa SET ?;
                `
            , [data],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'Data berhasil ditambahkan',
                });
            });
            connection.release();
        })
    },
    // edit data mahasiswa
    editDatamahasiswa(req,res){
        let dataEdit = {
            nama : req.body.nama,
            nim : req.body.nim,
            tanggal_lahir : req.body.tanggal_lahir,
            tempat_lahir : req.body.tempat_lahir,
            tahun_masuk : req.body.tahun_masuk,
            kode_ps : req.body.kode_ps
        }
        let nim = req.body.nim
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                UPDATE mahasiswa SET ? WHERE nim = ?;
                `
            , [dataEdit, nim],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'data berhasil diedit',
                });
            });
            connection.release();
        })
    },
    // Delete data mahasiswa
    deleteDatamahasiswa(req,res){
        let nim = req.body.nim
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                DELETE FROM mahasiswa WHERE nim = ?;
                `
            , [nim],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'Data berhasil dihapus'
                });
            });
            connection.release();
        })
    }
}