const config = require('../database');
const mysql = require('mysql');
const pool = mysql.createPool(config);

pool.on('error',(err)=> {
    console.error(err);
});

module.exports ={
    // ambil data sks mahasiswa
    getDatasks(req,res){
        let nim = req.body.nim
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                SELECT sum(mata_kuliah.sks) as jumlah_sks FROM mata_kuliah, krs WHERE mata_kuliah.kode_mk = krs.kode_mk AND krs.nim = ?;
                `
            , [kode_ps],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'Data berhasil diambil'
                });
            });
            connection.release();
        })
    },
    // ambil data mata kuliah per program studi
    getDatamk(req,res){
        let kode_ps = req.body.kode_ps
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                SELECT * FROM mata_kuliah LEFT JOIN program_studi ON mata_kuliah.kode_ps=program_studi.kode_ps WHERE mata_kuliah.kode_ps = ?;
                `
            , [kode_ps],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'Data berhasil diambil'
                });
            });
            connection.release();
        })
    }
}