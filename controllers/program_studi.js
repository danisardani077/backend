const config = require('../database');
const mysql = require('mysql');
const pool = mysql.createPool(config);

pool.on('error',(err)=> {
    console.error(err);
});

module.exports ={
    // Ambil data semua program_studi
    getDataprogram_studi(req,res){
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                SELECT * FROM program_studi;
                `
            , function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'data berhasil diambil',
                    data: results 
                });
            });
            connection.release();
        })
    },
    // Menambah data program_studi
    addDataprogram_studi(req,res){
        let data = {
            kode_ps : req.body.kode_ps,
            nama : req.body.nama,
        }
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                INSERT INTO program_studi SET ?;
                `
            , [data],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'Data berhasil ditambahkan',
                });
            });
            connection.release();
        })
    },
    // edit data program_studi
    editDataprogram_studi(req,res){
        let dataEdit = {
            kode_ps : req.body.kode_ps,
            nama : req.body.nama,
        }
        let kode_ps = req.body.kode_ps
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                UPDATE program_studi SET ? WHERE kode_ps = ?;
                `
            , [dataEdit, kode_ps],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'data berhasil diedit',
                });
            });
            connection.release();
        })
    },
    // Delete data program_studi
    deleteDataprogram_studi(req,res){
        let kode_ps = req.body.kode_ps
        pool.getConnection(function(err, connection) {
            if (err) throw err;
            connection.query(
                `
                DELETE FROM program_studi WHERE kode_ps = ?;
                `
            , [kode_ps],
            function (error, results) {
                if(error) throw error;  
                res.send({ 
                    success: true, 
                    message: 'Data berhasil dihapus'
                });
            });
            connection.release();
        })
    }
}